import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { SignInComponent } from './components/auth/sign-in/sign-in.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { SignOutComponent } from './components/auth/sign-out/sign-out.component';
import { MasterComponent } from './components/layout/master/master.component';

// Guard
import { LoggedInGuard } from './guards/logged-in.guard';

const routes: Routes = [
  { path: '', component: SignInComponent },
  { path: 'signin', component: SignInComponent },
  { path: 'signout', component: SignOutComponent },
  { path: '', component: MasterComponent, canActivate: [LoggedInGuard], children: [
    { path: 'dashboard', component: DashboardComponent },
  ]},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

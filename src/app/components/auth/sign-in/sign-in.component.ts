import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';
import { TokenService } from '../../../services/token.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  invalidLogin = false;
  @Input() FormControl: FormControl;
  loginForm: FormGroup;

  constructor(
    private _authService: AuthService,
    private _tokenService: TokenService,
    private _router: Router,
    private _fb: FormBuilder
  ) {
    this.loginForm = _fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    });

    if (this._tokenService.isAuthenticated()) {
      this._router.navigate(['/dashboard']);
    }
  }

  ngOnInit() {
  }

  login(loginForm) {
    this._authService.login(this.loginForm)
    .map( data => {
      return data.json();
    })
    .subscribe(
      token => {
        this._tokenService.setAccessToken(token.access_token);
      },
      erros => {
        this.invalidLogin = true;
      },
      () => {
        this._router.navigate(['/dashboard']);
      }
    );
  }

}

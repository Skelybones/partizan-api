import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../../../services/token.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.scss']
})
export class SignOutComponent implements OnInit {

  constructor(
    private _tokenService: TokenService,
    private _authService: AuthService,
    private _router: Router
  ) {
    if (this._tokenService.isAuthenticated()) {
      this._authService.signOut();
    } else {
      this._router.navigate(['/signin']);
    }
  }

  ngOnInit() {
  }

}

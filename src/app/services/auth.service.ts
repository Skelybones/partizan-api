import { Injectable } from '@angular/core';
import { Headers, Response, Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

import { ApiService } from './api.service';
import { TokenService } from './token.service';

@Injectable()
export class AuthService {

  constructor(
    private _http: Http,
    private _api: ApiService,
    private _tokenService: TokenService,
    private _router: Router
  ) { }

  login(loginForm) {
    const formData = new FormData();
      formData.append('email', loginForm.value.email);
      formData.append('password', loginForm.value.password);
    return this._http.post(
      this._api.url('/auth/sign-in'),
      formData
    );
  }

  signOut() {
    this._tokenService.deleteAccessToken();
    this._tokenService.deleteRoleId();
    this._router.navigate(['/']);
  }

  getRoleID() {
    return localStorage.getItem('role_id');
  }

}

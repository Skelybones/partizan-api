import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {

  constructor() { }

  public url(params) {
    return 'http://partizan:8000' + params;
  }

}
